resource_is_present:
    nsx_alb.alb.vs_vip.present:
    - name: Idem-vsvip13_now
      vip:
      - auto_allocate_floating_ip: false
        auto_allocate_ip: false
        auto_allocate_ip_type: V4_ONLY
        avi_allocated_fip: false
        avi_allocated_vip: false
        enabled: false
        ip_address:
          addr: 10.10.26.25
          type: V4
        prefix_length: 32
