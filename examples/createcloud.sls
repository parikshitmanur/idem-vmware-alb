resource_is_present:
    nsx_alb.alb.cloud.present:
    - dhcp_enabled: true
      enable_vip_static_routes: false
      license_type: LIC_CORES
      mtu: 1501
      name: VCenter Cloud
      prefer_static_routes: true
      vcenter_configuration:
        datacenter: blr-01-vc13
        management_network: blr-01-avi-dev-IntMgmt
        password: AviUser1234!.
        privilege: WRITE_ACCESS
        username: aviuser1
        vcenter_url: blr-01-vc13.oc.vmware.com
        content_lib:
          id: "8bc7da0a-b866-4b70-bdad-d5b66282ea69"
      vtype: CLOUD_VCENTER
