vsvip_creation:
    nsx_alb.alb.vs_vip.present:
    - name: Idem-vsvip13_now123
      east_west_placement: false
      vip:
      - auto_allocate_floating_ip: false
        auto_allocate_ip: false
        auto_allocate_ip_type: V4_ONLY
        avi_allocated_fip: false
        avi_allocated_vip: false
        enabled: true
        ip_address:
          addr: 10.10.26.21
          type: V4
        prefix_length: 32
        vip_id: '0'
virtualservice-creation:
  nsx_alb.alb.virtual_service.present:
    - name: Idem_vs_now123
      cloud_type: CLOUD_NONE
      enabled: true
      services:
        - enable_ssl: false
          port: 80
          port_range_end: 87
      ssl_sess_cache_avg_size: 1024
      traffic_enabled: false
      type: VS_TYPE_NORMAL
      vsvip_ref: ${nsx_alb.alb.vs_vip:vsvip_creation:name}
