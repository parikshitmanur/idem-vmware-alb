pool_creation:
  nsx_alb.alb.pool.present:
  - name: Pool_idem_new1224
    default_server_port: 80
    analytics_profile_ref: System-Analytics-Profile
    health_monitor_refs:
      - Idem-HTTP-now
      - System-HTTP
  - servers:
    - enabled: true
      hostname: poolserver
      ip:
        addr: 1.2.3.5
        type: V4
      ratio: 2
      resolve_server_by_dns: false
      rewrite_host_header: false
      static: false
      verify_network: true
