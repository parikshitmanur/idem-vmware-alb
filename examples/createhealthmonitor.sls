resource_is_present:
    nsx_alb.alb.health_monitor.present:
    - name: Idem-HTTP-now
      is_federated: false
      send_interval: 10
      receive_timeout: 2
      successful_checks: 4
      failed_checks: 4
      type: HEALTH_MONITOR_HTTP
      http_monitor:
        http_request: HEAD / HTTP/1.0
        http_response_code:
          - HTTP_2XX
          - HTTP_3XX
          - HTTP_5XX
        exact_http_request: true
